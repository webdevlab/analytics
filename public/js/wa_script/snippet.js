(function(w,d,tn,u,fn,e,pe){
    w['WebAnalytics'] = fn;
    w[fn] = w[fn] || function() {
        (w[fn].q = w[fn].q || []).push(arguments)
    };
    e=d.createElement(tn);
    pe=d.getElementsByTagName(tn)[0];
    e.async=1;
    e.src=u;
    pe.parentNode.insertBefore(e,pe)
})(window,document,'script','//analytics.loc/js/wa_script/script.js','wa');

wa('create', '1564978556');
wa('send', 'page-visit');
