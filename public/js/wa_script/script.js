(function(w){
    var fn = w['WebAnalytics'],
        queued = w[fn].q,
        jq = false;

    initJQ();

    w[fn] = {
        sid: null,
        create: function (params){
            this.sid = params[0];
        },
        send: function (params){
            sendRequest(
                $.ajax({
                    type: "POST",
                    url: "http://analytics.loc/receiver/",
                    data: {sid: this.sid, event: params[0]}
                })
            );
        }
    };

    if(queued.length) {
        for (var i = 0; i < queued.length; i++) {
            var q = Array.prototype.slice.call(queued[i]);
            w[fn][q[0]](q.slice(1, q.length));
        }
    }

    function initJQ(){
        if (typeof(jQuery) == 'undefined') {
            if(!jq){
                jq = true;
                var elem = document.createElement('script');
                var pelem = document.getElementsByTagName('script')[0];
                elem.async=1;
                elem.src='//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';
                pelem.parentNode.insertBefore(elem,pelem);
            }
            setTimeout(initJQ, 50);
        } else {
            jq = true;
        }
    }

    function sendRequest(callback){
        var timeout = null;
        if(jq){
            clearTimeout(timeout);
            return function(){
                callback
            };
        } else {
            timeout = setTimeout(sendRequest(callback), 50);
        }
    }

})(window);