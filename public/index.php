<?php

use Phalcon\Mvc\Application;

defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
define('APP_PATH', realpath('..'));

$config = include APP_PATH . "/apps/config/config.php";

if (in_array(APPLICATION_ENV, array('development', 'production'))) {
    $debug = new \Phalcon\Debug();
    $debug->listen();
}

/**
 * Include services
 */
require APP_PATH . "/apps/config/services.php";

/**
 * Include loaders
 */
require APP_PATH . "/apps/config/loader.php";

/**
 * Handle the request
 */
$application = new Application();

/**
 * Assign the DI
 */
$application->setDI($di);

/**
 * Include modules
 */
require APP_PATH . "/apps/config/modules.php";

echo $application->handle()->getContent();
