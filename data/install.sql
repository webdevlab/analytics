DROP TABLE IF EXISTS `user_group`;
CREATE TABLE `user_group` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `alias` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

INSERT INTO `user_group` (`id`, `name`, `alias`) VALUES (1, 'Administrator', 'admin');
INSERT INTO `user_group` (`id`, `name`, `alias`) VALUES (2, 'User', 'user');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` CHAR(60) NOT NULL,
  `groupId` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `groupId` (`groupId`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

INSERT INTO `user` (`id`, `username`, `email`, `password`, `groupId`) VALUES
  (1, 'DemoAdmin', 'dangelzm@gmail.com', '$2a$08$MlxtQ3/rNfPqz4Vi1a.boukykDzJNdwfK2Pe.TMvTifYzrijoiz8C', 1);

DROP TABLE IF EXISTS `site`;
CREATE TABLE `site` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `sitename` VARCHAR(250) NOT NULL,
  `url` VARCHAR(250) NOT NULL,
  `user_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `FK__user` (`user_id`),
  CONSTRAINT `FK__user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

CREATE TABLE `stats` (
  `site_id` INT(11) NOT NULL,
  `stat_time` INT(11) NOT NULL,
  `event` VARCHAR(50) NOT NULL,
  `counter` BIGINT(20) NOT NULL DEFAULT '1',
  UNIQUE INDEX `stat_unique` (`stat_time`, `site_id`, `event`),
  INDEX `FK_stats_site` (`site_id`),
  CONSTRAINT `FK_stats_site` FOREIGN KEY (`site_id`) REFERENCES `site` (`id`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;
