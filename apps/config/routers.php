<?php

$router = new Phalcon\Mvc\Router(false);

$router->setDefaultModule("frontend");

// 404 page
$router->notFound(array(
    'controller'    => 'error',
    'action'        => 'show404'
));

$router->add('/receiver', array(
    'controller'    => 'receiver',
    'action'        => 'save'
));

$router->add("/", array(
    'controller' => 'home',
    'action'     => 'index',
))->setName('site');

$router->add("/about", array(
    'controller' => 'static-page',
    'action'     => 'about',
))->setName('about');

$router->add("/registration", array(
    'controller' => 'user',
    'action'     => 'registration',
))->setName('user-registration');

// Platform routers
$router->add("/platform/logout", array(
    'module'     => 'platform',
    'controller' => 'dashboard',
    'action'     => 'logout',
));

$router->add("/platform", array(
    'module'     => 'platform',
    'controller' => 'dashboard',
    'action'     => 'index',
))->setName('platform');

$router->add("/platform/sites", array(
    'module'     => 'platform',
    'controller' => 'sites',
    'action'     => 'index',
))->setName('platform-sites');

$router->add("/platform/sites/view/:int", array(
    'module'     => 'platform',
    'controller' => 'sites',
    'action'     => 'view',
    'sid'        => 1,
));

$router->add("/platform/sites/add", array(
    'module'     => 'platform',
    'controller' => 'sites',
    'action'     => 'add',
));

// Backend routers
$router->add("/admin", array(
    'module'     => 'backend',
    'controller' => 'index',
    'action'     => 'index',
));

$router->add("/admin/logout", array(
    'module'     => 'backend',
    'controller' => 'index',
    'action'     => 'logout',
));

$router->add("/admin/users", array(
    'module'     => 'backend',
    'controller' => 'users',
    'action'     => 'index',
));

$router->add("/admin/users/add", array(
    'module'     => 'backend',
    'controller' => 'users',
    'action'     => 'add',
));

$router->add("/admin/users/:int", array(
    'module'     => 'backend',
    'controller' => 'users',
    'action'     => 'byId',
    'uid'        => 1,
));

// Конечные косые черты будут автоматически удалены
$router->removeExtraSlashes(true);

return $router;