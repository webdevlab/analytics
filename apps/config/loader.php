<?php

use Phalcon\Loader;

$loader = new Loader();

$loader->registerNamespaces(array(
    'Webanalytics\Frontend\Controllers' => __DIR__ . '/../modules/frontend/controllers/',
    'Webanalytics\Frontend\Forms'        => __DIR__ . '/../modules/frontend/forms/',
    'Webanalytics\Backend\Controllers'  => __DIR__ . '/../modules/backend/controllers/',
    'Webanalytics\Backend\Forms'        => __DIR__ . '/../modules/backend/forms/',
    'Webanalytics\Platform\Controllers'  => __DIR__ . '/../modules/platform/controllers/',
    'Webanalytics\Platform\Forms'        => __DIR__ . '/../modules/platform/forms/',
    'Webanalytics\Models'               => __DIR__ . '/../common/models/',
    'Webanalytics'                      => __DIR__ . '/../library/'

));

$loader->register();