<?php

$application->registerModules(array(
    'frontend' => array(
        'className' => 'Webanalytics\Frontend\Module',
        'path' => __DIR__ . '/../modules/frontend/Module.php'
    ),
    'backend' => array(
        'className' => 'Webanalytics\Backend\Module',
        'path' => __DIR__ . '/../modules/backend/Module.php'
    ),
    'platform' => array(
        'className' => 'Webanalytics\Platform\Module',
        'path' => __DIR__ . '/../modules/platform/Module.php'
    )
));
