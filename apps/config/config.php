<?php

$env = array(
    'production'  => array(
        'app_host' => 'analytics.webdevlab.com.ua',
        'database'    => array(
            'host'     => '127.0.0.1',
            'username' => 'webanalytic',
            'password' => '532453',
            'dbname'   => 'web_analytics',
            'charset'  => 'utf8'
        )
    ),
    'development' => array(
        'app_host' => 'analytics.loc',
        'database'    => array(
            'host'     => 'localhost',
            'username' => 'dangelzm',
            'password' => '532453',
            'dbname'   => 'web_analytics',
            'charset'  => 'utf8'
        )
    ),
);

$app_config = array(
    'database' => $env[APPLICATION_ENV]['database'],
    'app_host' => $env[APPLICATION_ENV]['app_host'],
    'security' => array(
        'workFactor' => 7,
        'dsk' => 532453
    )
);

return new \Phalcon\Config($app_config);