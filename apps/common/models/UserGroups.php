<?php
namespace Webanalytics\Models;

use Phalcon\Mvc\Model;

class UserGroups extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Alias
     * @var string
     */
    public $alias;

    /**
     * Define relationships to Users
     */
    public function initialize()
    {
        $this->setSource("user_group");

        $this->hasMany('id', 'Webshop\Models\Users', 'groupId', array(
            'alias' => 'users',
            'foreignKey' => array(
                'message' => 'Group cannot be deleted because it\'s used on Users'
            )
        ));
    }
}