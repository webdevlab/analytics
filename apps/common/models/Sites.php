<?php

namespace Webanalytics\Models;

class Sites extends \Phalcon\Mvc\Model
{
    public $id;
    public $sitename;
    public $url;
    public $user_id;

    public function initialize()
    {
        $this->setSource("site");

        $this->hasMany('id', 'Webanalytics\Models\SiteStats', 'site_id', array(
            'alias' => 'stats',
            'reusable' => true
        ));

        $this->belongsTo('user_id', 'Webanalytics\Models\Users', 'id', array(
            'alias' => 'user',
            'reusable' => true
        ));
    }

    public static function findWithStats($modelsManager, $event, $params=null) {
        $builder = $modelsManager->createBuilder();
        $builder->columns('site.*, stats.*')
            ->addFrom('Webanalytics\Models\Sites', 'site')
            ->join('Webanalytics\Models\SiteStats', 'site.id = stats.site_id', 'stats')
            ->where('stats.event = :event:', array('event' => $event));
        if($params){
            foreach ($params as $key => $value) {
                $builder->andWhere('site.' . $key . ' = :' . $key . ':', array($key => $value));
            }
        }
        return $builder->getQuery()->execute();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSitename()
    {
        return $this->sitename;
    }

    /**
     * @param mixed $sitename
     */
    public function setSitename($sitename)
    {
        $this->sitename = $sitename;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }


}