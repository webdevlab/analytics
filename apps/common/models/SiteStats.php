<?php

namespace Webanalytics\Models;

class CategoriesDescription extends \Phalcon\Mvc\Model
{
    public $site_id;
    public $event;
    public $stat_time;
    public $counter;

    public function initialize()
    {
        $this->setSource("stats");

        $this->belongsTo('site_id', 'Webanalytics\Models\Sites', 'id', array(
            'alias' => 'sites',
            'reusable' => true
        ));
    }

    /**
     * @return mixed
     */
    public function getSiteId()
    {
        return $this->site_id;
    }

    /**
     * @param mixed $site_id
     */
    public function setSiteId($site_id)
    {
        $this->site_id = $site_id;
    }

    /**
     * @return mixed
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * @param mixed $counter
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;
    }

    /**
     * @return mixed
     */
    public function getStatTime()
    {
        return $this->stat_time;
    }

    /**
     * @param mixed $stat_time
     */
    public function setStatTime($stat_time)
    {
        $this->stat_time = $stat_time;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param mixed $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

}