<?php
namespace Webanalytics\Auth;

use Phalcon\Mvc\User\Component,
    Webanalytics\Models\Users;

class Auth extends Component
{
    public function check($credentials, $redirect)
    {

        // Check if the user exist
        $user = Users::findFirstByEmail($credentials['email']);
        if ($user == false) {
            $this->flashSession->error('Wrong email/password combination');
            return $this->response->redirect($redirect);
        }

        // Check the password
        if (!$this->security->checkHash($credentials['password'], $user->password)) {
            $this->flashSession->error('Wrong email/password combination');
            return $this->response->redirect($redirect);
        }

        $this->session->set('identity', array(
            'id' => $user->id,
            'name' => $user->username,
            'group' => $user->group->alias,
            'group_name' => $user->group->name,
        ));

        return $this->response->redirect($redirect);
    }

    public function getIdentity($param = null)
    {
        $identity = $this->session->get('identity');
        if($param){
            switch($param){
                case 'id';
                case 'name';
                case 'group';
                    $result = $identity[$param];
                break;
                default;
                    $result = $identity;
                break;
            }
        }
        return $result;
    }

    public function getUsername()
    {
        return $this->getIdentity('name');
    }

    public function getUserGroup()
    {
        return $this->getIdentity('group');
    }

    public function remove()
    {
        $this->session->destroy();
    }
}