<?php
namespace Webanalytics\Acl;

use Phalcon\Mvc\User\Component;

class Acl extends Component
{
    private $privateResources = array(
        'backend' => array(
            'users' => array(
                'index',
                'add'
            ),
        ),
        'platform' => array(

        )
    );

    public function isPrivate($module, $controllerName, $actionName)
    {
        if(isset($this->privateResources[$module])){
            if(array_key_exists($controllerName, $this->privateResources[$module])){
                if(in_array($actionName,$this->privateResources[$module][$controllerName])){
                    $private = true;
                } else {
                    $private = false;
                }
            } else {
                $private = false;
            }
        } else {
            $private = false;
        }

        return $private;
    }
}
 