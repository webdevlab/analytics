<head>
    {{ partial("partials/meta") }}
    {{ get_title() }}
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700|PT+Serif+Caption:400,400italic|Scada:400italic,700italic,400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    {{ assets.outputCss('header_css') }}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    {{ assets.outputJs('header_js') }}
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
            filter: none;
        }
    </style>
    <![endif]-->
</head>
