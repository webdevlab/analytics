<section class="tech-stack">
    <div class="container">
        <div class="row">
            <div class="section-title">What we use in our work</div>
            <div class="col-md-10 col-sm-12 col-md-offset-1">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-sm-6 info">
                                    <h4>PHP</h4>
                                    <p>
                                        PHP is a popular general-purpose scripting language that is especially suited to
                                        web development. Fast, flexible and pragmatic, PHP powers everything from your
                                        blog to the most popular websites in the world.
                                    </p>
                                </div>
                                <div class="col-sm-6 holder">
                                    <img src="img/promo/php_code_lines.jpg" alt="PHP code lines">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-6 info">
                                    <h4>MySQL</h4>
                                    <p>
                                        The world's most popular open source database.
                                        Whether you are a fast growing web property, technology ISV or large enterprise,
                                        MySQL can cost-effectively help you deliver high performance, scalable database
                                        applications.
                                    </p>
                                </div>
                                <div class="col-sm-6 holder">
                                    <img src="img/promo/mysql_visual_table_editing.png" alt="MySQL visual table editing">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-6 info">
                                    <h4>Git</h4>
                                    <p>
                                        Git is a free and open source distributed version control system designed to
                                        handle everything from small to very large projects with speed and efficiency.
                                    </p>
                                </div>
                                <div class="col-sm-6 holder">
                                    <img src="img/promo/git-branching.png" alt="Git branching">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>