<section class="content-header container">
    <h1>Регистрация</h1>
</section>
<section class="content container">
    {{ content() }}

    <form class="form-horizontal" role="form" method="post">
        <div class="form-group">
            {{ form.label('username', ['class':'col-sm-2 control-label']) }}
            <div class="col-sm-10">
                {{ form.render('username') }}
                {{ form.messages('username') }}
            </div>
        </div>
        <div class="form-group">
            {{ form.label('email', ['class':'col-sm-2 control-label']) }}
            <div class="col-sm-10">
                {{ form.render('email') }}
                {{ form.messages('email') }}
            </div>
        </div>
        <div class="form-group">
            {{ form.label('password', ['class':'col-sm-2 control-label']) }}
            <div class="col-sm-10">
                {{ form.render('password') }}
                {{ form.messages('password') }}
            </div>
        </div>
        <div class="form-group">
            {{ form.label('confirmPassword', ['class':'col-sm-2 control-label']) }}
            <div class="col-sm-10">
                {{ form.render('confirmPassword') }}
                {{ form.messages('confirmPassword') }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                {{ form.render('Зарегистрировать') }}
            </div>
        </div>
        {{ form.render('csrf', ['value': security.getToken()]) }}
        {{ form.messages('csrf') }}
    </form>
</section>


