{{ partial("partials/head") }}
<body>
    {{ partial("partials/header") }}
    <div id="contentContainer">
        {{ content() }}
    </div>
    {{ partial("partials/footer") }}
</body>