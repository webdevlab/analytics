<?php
namespace Webanalytics\Frontend\Forms;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Submit,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Validation\Validator\Email,
    Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Identical,
    Phalcon\Validation\Validator\StringLength,
    Phalcon\Validation\Validator\Confirmation;

class SignUpForm extends Form
{

    public function initialize()
    {

        $username = new Text('username', array('class' => 'form-control','placeholder' => 'Username', 'maxlength' => 16));
        $username->addValidator(
            new PresenceOf(array('message' => 'Имя акаунта обезательное поле')
            ));
        $username->setLabel('Имя акаунта');
        $this->add($username);

        $email = new Text('email', array('class' => 'form-control','placeholder' => 'username@example.com'));
        $email->addValidators(array(
            new PresenceOf(array('message' => 'Email обезательное поле')),
            new Email(array('message' => 'Email не валидный'))
        ));
        $email->setLabel('Email');
        $this->add($email);

        $password = new Password('password', array('class' => 'form-control'));
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'Пароль обязательное поле'
            )),
            new StringLength(array(
                'min' => 6,
                'messageMinimum' => 'Пароль слишком короткий. Минимум 6 символов'
            )),
            new Confirmation(array(
                'message' => 'Пароль не соответсвует подтверждению',
                'with' => 'confirmPassword'
            ))
        ));
        $password->setLabel('Пароль');
        $this->add($password);

        $confirmPassword = new Password('confirmPassword', array('class' => 'form-control'));
        $confirmPassword->setLabel('Подтвердите пароль');
        $confirmPassword->addValidators(array(
            new PresenceOf(array(
                'message' => 'Подтверждение пароля обязательно'
            ))
        ));
        $this->add($confirmPassword);

        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'Ошибка CSRF валидации!!!'
        )));
        $this->add($csrf);

        $this->add(new Submit('Зарегистрировать', array(
            'class' => 'btn btn-primary'
        )));

    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }

}