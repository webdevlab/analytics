<?php

namespace Webanalytics\Frontend\Controllers;

class ReceiverController extends ControllerBase
{

    public function saveAction(){
        $this->view->disable();
        $response = new \Phalcon\Http\Response();
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
        $response->sendHeaders();
        if ($this->request->isPost()) {
            $sql = "INSERT INTO stats (site_id, event, stat_time)"
                . " VALUES (?, ?, ?)"
                . " ON DUPLICATE KEY UPDATE "
                . " stats.counter = stats.counter + 1";
            $success = $this->db
                ->execute($sql,
                    array(1, 'page-visit', mktime(date("H"),0, 0, date("n"), date("j"), date("Y")))
                );

            if (!$success) {
                $response->setContent(json_encode(['msg'=>'Error event not saved!']));
            } else {
                $response->setContent(json_encode(['msg'=>$success]));
            }
            return $response;
        } else {
            $response->setContent(json_encode(['msg'=>'Need POST request!!!']));
            return $response;
        }
    }

}

