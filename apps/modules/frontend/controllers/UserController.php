<?php

namespace Webanalytics\Frontend\Controllers;

use Phalcon\Tag,
    Webanalytics\Models\Users,
    Webanalytics\Frontend\Forms\SignUpForm;

class UserController extends ControllerBase
{

    public function registrationAction()
    {
        $form = new SignUpForm(null);

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {

                $user = new Users();
                $user->assign(array(
                    'username' => $this->request->getPost('username', 'striptags'),
                    'email' => $this->request->getPost('email', 'email'),
                    'password' => $this->security->hash($this->request->getPost('password')),
                    'groupId' => 2, //user
                ));

                if (!$user->save()) {
                    $this->flash->error($user->getMessages());
                } else {
                    $this->flash->success("Пользователь был успешно создан!");

                    $this->auth->check(array(
                        'email' => $user->email,
                        'password' => $this->request->getPost('password'),
                    ),
                    'platform'
                    );
                }
            }
        }

        $this->view->form = $form;

    }

}

