<?php

namespace Webanalytics\Frontend\Controllers;

use Phalcon\Mvc\View;

class ErrorController extends ControllerBase
{
    public function show404Action($msg = "Запрашиваемая вами страница не найдена!"){
        $this->view->error_msg = $msg;
        $this->response->setStatusCode(404, "Not Found");
    }

}
