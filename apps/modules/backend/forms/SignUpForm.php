<?php
namespace Webanalytics\Backend\Forms;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Submit,

    Phalcon\Validation\Validator\PresenceOf;

class SignUpForm extends Form
{

    public function initialize()
    {

        $username = new Text('username', array('maxlength' => 16));
        $username->addValidator(new PresenceOf(array(
            'message' => 'User name is required'
        )));
        $username->setLabel('Username');
        $this->add($username);

        $password = new Password('password');
        $password->addValidator(new PresenceOf(array(
            'message' => 'Password is required'
        )));
        $password->setLabel('Password');
        $this->add($password);

        $this->add(new Submit('Сохранить', array(
            'class' => 'btn btn-success'
        )));

    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }

}