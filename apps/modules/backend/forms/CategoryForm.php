<?php
namespace Webanalytics\Backend\Forms;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\TextArea,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Submit,
    Phalcon\Validation\Validator\PresenceOf,
    Webanalytics\Models\Languages,
    Webanalytics\Models\CategoriesDescription;

class CategoryForm extends Form
{
    protected $edit;
    public function initialize($entity = null, $options = null)
    {
        $languages = Languages::find('status = 1');

        if (isset($options['edit']) && $options['edit']) {
            $this->edit = true;
            $id = new Hidden('id');
        } else {
            $this->edit = false;
            $id = new Text('id');
        }
        $this->add($id);

        foreach($languages as $lang){
            if($this->edit) {
                $cDescription = $entity->getDescription('lang_id=' . $lang->lang_id)->getFirst();
            }
            $name = new Text('category_description[' . $lang->lang_id . '][name]', array('class' => 'form-control'));
            $name->addValidator(
                new PresenceOf(array('message' => 'Название категории обязательно для заполнения')
            ));
            $name->setLabel('Название категории:<span class="require">*</span>');
            if($this->edit){
                $name->setDefault($cDescription->name);
            };
            $this->add($name);

            $meta_title = new Text('category_description[' . $lang->lang_id . '][meta_title]', array('class' => 'form-control'));
            $meta_title->setLabel('Мета-тег "Заголовок":');
            if($this->edit) {
                $meta_title->setDefault($cDescription->meta_title);
            }
            $this->add($meta_title);

            $meta_description = new Text('category_description[' . $lang->lang_id . '][meta_description]', array('class' => 'form-control'));
            $meta_description->setLabel('Мета-тег "Описание":');
            if($this->edit) {
                $meta_description->setDefault($cDescription->meta_description);
            }
            $this->add($meta_description);

            $meta_keyword = new Text('category_description[' . $lang->lang_id . '][meta_keyword]', array('class' => 'form-control'));
            $meta_keyword->setLabel('Мета-тег "Ключевые слова":');
            if($this->edit) {
                $meta_keyword->setDefault($cDescription->meta_keyword);
            }
            $this->add($meta_keyword);

            $description = new TextArea('category_description[' . $lang->lang_id . '][description]', array('class' => 'form-control'));
            $description->setLabel('Описание:');
            if($this->edit) {
                $description->setDefault($cDescription->description);
            }
            $this->add($description);
        }

        $parent = new Select('parent_id', CategoriesDescription::find('lang_id=' . 1), array(
            'class' => 'form-control',
            'using' => array(
                'category_id',
                'name'
            ),
            'useEmpty' => true,
            'emptyText' => 'Выберите родительскую категорию',
            'emptyValue' => '0'
        ));
        $parent->setLabel('Родительская категория');
        $this->add($parent);

        $sort_order = new Numeric('sort_order', array(
            'class' => 'form-control',
        ));
        $sort_order->setLabel('Порядок сортировки:');
        $this->add($sort_order);

        $status = new Check('status');
        $status->setLabel('Категория активна');
        $this->add($status);

        $this->add(new Submit('Сохранить', array(
            'class' => 'btn btn-success btn-sm'
        )));

    }

    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }

}