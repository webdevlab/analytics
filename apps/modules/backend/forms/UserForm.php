<?php
namespace Webanalytics\Backend\Forms;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Submit,
    Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email,
    Webanalytics\Models\UserGroups;

class UserForm extends Form
{

    public function initialize($entity = null, $options = null)
    {

        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $id = new Hidden('id');
        } else {
            $id = new Text('id');
        }
        $this->add($id);

        $username = new Text('username', array('class' => 'form-control','placeholder' => 'Username','maxlength' => 16));
        $username->addValidator(
            new PresenceOf(array('message' => 'User name is required')
        ));
        $username->setLabel('Username');
        $this->add($username);

        $email = new Text('email', array('class' => 'form-control','placeholder' => 'username@example.com'));
        $email->addValidators(array(
            new PresenceOf(array('message' => 'The e-mail is required')),
            new Email(array('message' => 'The e-mail is not valid'))
        ));
        $email->setLabel('Email');
        $this->add($email);

        $password = new Password('password', array('class' => 'form-control'));
        $password->addValidator(
            new PresenceOf(array('message' => 'Password is required')
        ));
        $password->setLabel('Пароль');
        $this->add($password);

        $group = new Select('groupId', UserGroups::find(), array(
            'class' => 'form-control',
            'using' => array(
                'id',
                'name'
            ),
            'useEmpty' => true,
            'emptyText' => 'Выберите групу',
            'emptyValue' => ''
        ));
        $group->addValidator(
            new PresenceOf(array('message' => 'Group is required')
        ));
        $group->setLabel('Група');

        $this->add($group);

        $this->add(new Submit('Сохранить', array(
            'class' => 'btn btn-success'
        )));

    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }

}