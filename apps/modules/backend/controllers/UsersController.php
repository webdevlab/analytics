<?php

namespace Webanalytics\Backend\Controllers;

use Phalcon\Tag,
    Webanalytics\Models\Users,
    Webanalytics\Backend\Forms\UserForm;

class UsersController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->users = Users::find();
    }

    public function byIdAction()
    {
        $this->view->user = Users::findFirst($this->dispatcher->getParam('uid'));
    }

    public function addAction()
    {
        $form = new UserForm(null);
        $this->view->form = $form;

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {

                $user = new Users();
                $user->assign(array(
                    'username' => $this->request->getPost('username', 'striptags'),
                    'email' => $this->request->getPost('email', 'email'),
                    'password' => $this->security->hash($this->request->getPost('password')),
                    'groupId' => $this->request->getPost('groupId', 'int'),
                ));

                if (!$user->save()) {
                    $this->flash->error($user->getMessages());
                } else {
                    $this->flash->success("Пользователь был успешно создан!");
                    Tag::resetInput();
                }
            }
        }

    }

}

