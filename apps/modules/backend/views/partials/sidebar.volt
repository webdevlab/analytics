<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li>
            <a href="{{ url("admin") }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-folder"></i> <span>Каталог</span>
                <i class="fa fa-angle-right pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ url("admin/categories") }}"><i class="fa fa-angle-double-right"></i> Категории</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Товары</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Атрибуты</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Опции</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-folder"></i> <span>Система</span>
                <i class="fa fa-angle-right pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ url("admin/settings") }}"><i class="fa fa-wrench"></i> Настройки</a></li>
                <li><a href="{{ url("admin/users") }}"><i class="fa fa-users"></i> Пользователи</a></li>
                <li><a href="{{ url("admin/languages") }}"><i class="fa fa-language"></i> Языки</a></li>
            </ul>
        </li>
    </ul>
</section>
<!-- /.sidebar -->


