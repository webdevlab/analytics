<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Пользователи</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Пользователи</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    {{ content() }}

    <h4 class="page-header">
        <a class="btn bg-olive" href="{{ url('admin/users/add') }}" role="button">Добавить пользователя</a><br>
    </h4>

    {% for user in users %}
    {% if loop.first %}
    <div class="table-responsive">
        <table id="user-table" class="table table-hover table-bordered">
            <tr class="success">
                <th>#</th>
                <th>Id</th>
                <th>Username</th>
                <th>Email</th>
                <th>Група</th>
            </tr>
            {% endif %}
            <tr class="trow" data-uid="{{ user.id }}">
                <td>{{ loop.index }}</td>
                <td>{{ user.id }}</td>
                <td>{{ user.username }}</td>
                <td>{{ user.email }}</td>
                <td>{{ user.getGroup().name }}</td>
            </tr>
            {% if loop.last %}
        </table>
    </div>
    {% endif %}
    {% endfor %}

    <script>
        $(document).ready(function(){
            $("#user-table .trow").click(function(){
                window.location = '{{ url('admin/users/') }}' + $(this).data('uid');
            });
        });
    </script>
</section><!-- /.content -->

