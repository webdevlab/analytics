<body class="skin-blue">
    {{ partial("partials/nav") }}
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
            {{ partial("partials/sidebar") }}
        </aside>
        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            {{ content() }}
        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->
</body>