<?php
namespace Webanalytics\Platform\Controllers;

use Webanalytics\Platform\Forms\LoginForm;

class DashboardController extends ControllerBase
{

    public function indexAction()
    {
            if($this->session->has('identity') && $this->auth->getUserGroup() == 'user'){
                $this->view->identity = $this->session->get('identity');
            } else {
                $this->dispatcher->forward(array('action'=>'login'));
            }
    }

    public function loginAction()
    {
        $this->view->setTemplateBefore('clear');

        $form = new LoginForm();

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) == false) {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {

                $this->auth->check(array(
                    'email' => $this->request->getPost('email'),
                    'password' => $this->request->getPost('password'),
                ),
                'platform'
                );

                if($this->session->has('auth_redirect')){
                    $redirect = ltrim($this->session->get('auth_redirect'), '/');
                    $this->session->remove('auth_redirect');
                } else {
                    $redirect = 'platform';
                }

                return $this->response->redirect($redirect);
            }
        }

        $this->view->form = $form;
    }

    public function logoutAction()
    {
        $this->auth->remove();

        return $this->response->redirect('platform');
    }

}

