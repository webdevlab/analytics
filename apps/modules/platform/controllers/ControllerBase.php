<?php

namespace Webanalytics\Platform\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

class ControllerBase extends Controller
{
    public function initialize()
    {
        $this->view->setTemplateBefore('main');
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $controllerName = $dispatcher->getControllerName();
        $actionName = $dispatcher->getActionName();

        if ($this->acl->isPrivate('platform', $controllerName, $actionName)) {

            // Get the current identity
            $identity = $this->session->get('identity');

            // If there is no identity available the user is redirected to index/index
            if (!is_array($identity) || ($identity['group']!= 'user')) {

                $this->flash->notice('You don\'t have access to this module: private');
                $this->session->set('auth_redirect', $this->router->getRewriteUri());

                $dispatcher->forward(array(
                    'controller' => 'dashboard',
                    'action' => 'index'
                ));
                return false;
            }
        } 
    }

    public function preparePostForValidation($post, $level_key = null){
        $flat_array = array();
        foreach($post as $key=>$value){
            if(is_array($value)){
                $key = ($level_key) ? $level_key . '[' . $key . ']'  : $key;
                $flat_array = array_merge($flat_array, $this->preparePostForValidation($value, $key));
            } else {
                if($level_key){
                    $flat_array[$level_key.'['.$key.']'] = $value;
                } else {
                    $flat_array[$key] = $value;
                }
            }
        }

        return $flat_array;
    }

}
