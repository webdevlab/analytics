<?php

namespace Webanalytics\Platform\Controllers;

use Phalcon\Tag,
    Webanalytics\Models\Sites,
    Webanalytics\Platform\Forms\SiteForm;

class SitesController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->sites = Sites::find();
    }

    public function viewAction($sid)
    {
        $this->view->setVars(array(
            'site' => Sites::findFirst($sid),
            'platform_host' => $this->config->app_host
        ));
    }

    public function addAction()
    {
        $form = new SiteForm(null);

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
                $site = new Sites();
                $site->assign(array(
                    'sitename' => $this->request->getPost('sitename', 'striptags'),
                    'url' => $this->request->getPost('url'),
                    'user_id'=> $this->auth->getIdentity('id')
                ));

                if (!$site->save()) {
                    $this->flash->error($site->getMessages());
                } else {
                    $this->flash->success("Сайт был успешно добавлен в систему!");
                    return $this->response->redirect(array('for' => "platform-sites"));
                }
            }
        }

        $this->view->form = $form;

    }

}

