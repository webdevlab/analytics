<section class="content-header">
    <h1>Добавить пользователя</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url("platform") }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url("platform/sites") }}">Сайты</a></li>
        <li class="active">Добавить пользователя</li>
    </ol>
</section>
<section class="content">
    {{ content() }}

    <form class="form-horizontal" role="form" method="post">
        <div class="form-group">
            {{ form.label('sitename', ['class':'col-sm-2 control-label']) }}
            <div class="col-sm-10">
                {{ form.render('sitename') }}
                {{ form.messages('sitename') }}
            </div>
        </div>
        <div class="form-group">
            {{ form.label('url', ['class':'col-sm-2 control-label']) }}
            <div class="col-sm-10">
                {{ form.render('url') }}
                {{ form.messages('url') }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                {{ form.render('Сохранить') }}
            </div>
        </div>
    </form>
</section>


