<section class="content-header">
    <h1>Сайты</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Сайты</li>
    </ol>
</section>
<section class="content">
    {{ content() }}

    <h4 class="page-header">
        <a class="btn bg-olive" href="{{ url('platform/sites/add') }}" role="button">Добавить сайт</a><br>
    </h4>

    {% for site in sites %}
    {% if loop.first %}
    <div class="table-responsive">
        <table id="site-table" class="table table-hover table-bordered">
            <tr class="success">
                <th>#</th>
                <th>Название</th>
                <th>Адрес сайта</th>
            </tr>
            {% endif %}
            <tr class="trow" data-sid="{{ site.id }}">
                <td>{{ loop.index }}</td>
                <td>{{ site.sitename }}</td>
                <td>{{ site.url }}</td>
            </tr>
            {% if loop.last %}
        </table>
    </div>
    {% endif %}
    {% elsefor %}
        У вас пока нету зарегистрированых сайтов
    {% endfor %}

    <script>
        $(document).ready(function(){
            $("#site-table .trow").click(function(){
                window.location = '{{ url('platform/sites/view/') }}' + $(this).data('sid');
            });
        });
    </script>
</section>

