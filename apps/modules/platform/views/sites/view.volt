<section class="content-header">
    <h1>{{ site.sitename }}<small>({{ site.url }})</small></h1>
    <ol class="breadcrumb">
        <li><a href="{{ url("platform") }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url("platform/sites") }}">Сайты</a></li>
        <li class="active">{{ site.sitename }}</li>
    </ol>
</section>
<section class="content">
    {{ content() }}

    <div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#site_track_code" data-toggle="tab">Код отслеживания</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane nav-tabs-custom active" id="site_track_code">
                    <pre>{{ partial("partials/siteTrackCode", ['site': site, 'event': 'page-visit']) }}</pre>
                </div>
            </div>
        </div>
    </div>
</section>

