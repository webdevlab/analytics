<?php
date_default_timezone_set('Europe/Minsk');
$time = time();
echo date('H:i:s d/m/Y', $time) . '<br>'; // time
echo '-------- <br>';
echo mktime(date("H"),0, 0, date("n"), date("j"), date("Y")) . " : " . date('H:i:s d/m/Y', mktime(date("H"),0, 0, date("n"), date("j"), date("Y"))); // clear hour
echo '<br> -------- <br>';
echo mktime(0, 0, 0, date("n"), date("j"), date("Y")) . " : " . date('H:i:s d/m/Y', mktime(0, 0, 0, date("n"), date("j"), date("Y"))); // clear day
echo '<br> -------- <br>';
echo mktime(0, 0, 0, date("n"), 0, date("Y")) . " : " . date('H:i:s d/m/Y', mktime(0, 0, 0, date("n"), 1, date("Y"))); // clear month
?>