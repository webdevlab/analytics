<section class="content-header">
    <h1>Добавить пользователя</h1>
</section>
<section class="content">
    {{ content() }}

    <form class="form-horizontal" role="form" method="post">
        <div class="form-group">
            {{ form.label('username', ['class':'col-sm-2 control-label']) }}
            <div class="col-sm-10">
                {{ form.render('username') }}
                {{ form.messages('username') }}
            </div>
        </div>
        <div class="form-group">
            {{ form.label('email', ['class':'col-sm-2 control-label']) }}
            <div class="col-sm-10">
                {{ form.render('email') }}
                {{ form.messages('email') }}
            </div>
        </div>
        <div class="form-group">
            {{ form.label('password', ['class':'col-sm-2 control-label']) }}
            <div class="col-sm-10">
                {{ form.render('password') }}
                {{ form.messages('password') }}
            </div>
        </div>
        <div class="form-group">
            {{ form.label('groupId', ['class':'col-sm-2 control-label']) }}
            <div class="col-sm-10">
                {{ form.render('groupId') }}
                {{ form.messages('groupId') }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                {{ form.render('Сохранить') }}
            </div>
        </div>
    </form>
</section>


