<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Dashboard<small>все начинаеться здесь</small></h1>
</section>
<!-- Main content -->
<section class="content">
    {{ content() }}
    <h4 class="page-header">Cтатистика</h4>
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        150
                    </h3>
                    <p>
                        Новых заказов
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Подробней <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        53<sup style="font-size: 20px">%</sup>
                    </h3>
                    <p>
                        Показатель отказов
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Подробней <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        44
                    </h3>
                    <p>
                        Регистраций клиентов
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Подробней <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>
                        65
                    </h3>
                    <p>
                        Уникальных посетителей
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Подробней <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>
                        230
                    </h3>
                    <p>
                        Породаж
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios7-cart-outline"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Подробней <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3>
                        80<sup style="font-size: 20px">%</sup>
                    </h3>
                    <p>
                        Конверсия
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios7-briefcase-outline"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Подробней <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-teal">
                <div class="inner">
                    <h3>
                        14
                    </h3>
                    <p>
                        Уведомлений
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios7-alarm-outline"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Подробней <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-maroon">
                <div class="inner">
                    <h3>
                        160
                    </h3>
                    <p>
                        Продуктов
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios7-pricetag-outline"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Подробней <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->
</section><!-- /.content -->