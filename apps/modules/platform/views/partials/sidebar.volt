<section class="sidebar">
    <ul class="sidebar-menu">
        <li>
            <a href="{{ url("platform") }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        <li>
            <a href="{{ url("platform/sites") }}">
                <i class="fa fa-folder"></i> <span>Сайты</span>
                <i class="fa fa-angle-right pull-right"></i>
            </a>
        </li>
    </ul>
</section>


