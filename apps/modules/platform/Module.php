<?php

namespace Webanalytics\Platform;

use Phalcon\Loader,
    Phalcon\Mvc\View,
    Phalcon\Mvc\View\Engine\Volt as VoltEngine,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter,
    Phalcon\Mvc\ModuleDefinitionInterface;


class Module implements ModuleDefinitionInterface
{

    /**
     * Registers the module auto-loader
     */
    public function registerAutoloaders()
    {

    }

    /**
     * Registers the module-only services
     *
     * @param Phalcon\DI $di
     */
    public function registerServices($di)
    {

        /**
         * Read configuration
         */
        $config = include APP_PATH . "/apps/config/config.php";

        $di['view']->setViewsDir(__DIR__ . '/views/');

        $di->set('dispatcher', function() {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("Webanalytics\Platform\Controllers\\");
            return $dispatcher;
        });

        /**
         * Database connection is created based in the parameters defined in the configuration file
         */
        $di['db'] = function () use ($config) {
            return new DbAdapter(array(
                "host" => $config->database->host,
                "username" => $config->database->username,
                "password" => $config->database->password,
                "dbname" => $config->database->dbname,
                "charset" => $config->database->charset
            ));
        };

        $assets = $di['assets'];

        $assets
            ->collection('admin_header_css')
            ->addCss('//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css', false)
            ->addCss('//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css', false)
            ->addCss('css/ionicons.min.css')
            ->addCss('css/AdminLTE.css');

        $assets
            ->collection('admin_header_js')
            ->addJs('//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js', false)
            ->addJs('//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js', false);

        if($di['session']->get('identity')){
            $assets->collection('admin_header_js')->addJs('js/AdminLTE/app.js');
        }

    }

}
