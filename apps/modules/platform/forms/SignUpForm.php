<?php
namespace Webanalytics\Platform\Forms;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Submit,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Validation\Validator\PresenceOf;

class SignUpForm extends Form
{

    public function initialize()
    {

        $username = new Text('username', array('class' => 'form-control','placeholder' => 'Username','maxlength' => 16));
        $username->addValidator(
            new PresenceOf(array('message' => 'User name is required')
            ));
        $username->setLabel('Username');
        $this->add($username);

        $email = new Text('email', array('class' => 'form-control','placeholder' => 'username@example.com'));
        $email->addValidators(array(
            new PresenceOf(array('message' => 'Email обезательное поле')),
            new Email(array('message' => 'The e-mail is not valid'))
        ));
        $email->setLabel('Email');
        $this->add($email);

        $password = new Password('password', array('class' => 'form-control'));
        $password->addValidator(
            new PresenceOf(array('message' => 'Password is required')
            ));
        $password->setLabel('Пароль');
        $this->add($password);

        $this->add(new Submit('Зарегистрировать', array(
            'class' => 'btn btn-primary'
        )));

    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }

}