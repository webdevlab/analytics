<?php
namespace Webanalytics\Platform\Forms;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Submit,
    Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Url as UrlValidator;

class SiteForm extends Form
{

    public function initialize($entity = null, $options = null)
    {

        if (isset($options['edit']) && $options['edit']) {
            $id = new Hidden('id');
        } else {
            $id = new Text('id');
        }
        $this->add($id);

        $sitename = new Text('sitename', array('class' => 'form-control','placeholder' => 'Название','maxlength' => 50));
        $sitename->addValidator(
            new PresenceOf(array('message' => 'Название сайта обязательное поле')
        ));
        $sitename->setLabel('Название сайта');
        $this->add($sitename);

        $url = new Text('url', array('class' => 'form-control','placeholder' => 'http://example.com'));
        $url->addValidators(array(
            new PresenceOf(array('message' => 'URL сайта обязательное поле')),
            new UrlValidator(array('message' => 'Введенный адрес сайта не валидный'))
        ));
        $url->setLabel('URL сайта');
        $this->add($url);

        $this->add(new Submit('Сохранить', array(
            'class' => 'btn btn-success'
        )));

    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }

}